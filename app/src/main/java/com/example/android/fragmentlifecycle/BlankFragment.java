package com.example.android.fragmentlifecycle;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {

    @Override
    public void onAttach(Context context) {
        printLog("onAttach method");
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public BlankFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        printLog("OnCreateView called");
        return inflater.inflate(R.layout.fragment_blank, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        printLog("onActivityCreated method called");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {

        printLog("onStart method called");
        super.onStart();
    }

    @Override
    public void onResume() {

        printLog("onResume method called");
        super.onResume();
    }

    @Override
    public void onPause() {

        printLog("onPause method called");
        super.onPause();
    }

    @Override
    public void onDestroy() {
        printLog("onDestroy Called");

        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        printLog("onDestrotView Called");
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        printLog("OnDetach called");
        super.onDetach();
    }

    private void printLog(String s) {
        Log.e("fragment Method",s);
    }

    @Override
    public void onStop() {
        printLog("onStop called");
        super.onStop();
    }
}
